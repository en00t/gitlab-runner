.release_docker_images_base:
  stage: release
  extends:
  - .docker
  - .docker_qemu
  - .linux-dependency-checksums
  variables:
    PUBLISH_IMAGES: "true"
    PUSH_TO_DOCKER_HUB: "true"
    PUSH_TO_ECR_PUBLIC: "true"
  before_script:
  - source ci/touch_make_dependencies
  retry: 2

.release_docker_images:
  extends:
  - .release_docker_images_base
  dependencies:
  - 'ecr public token'
  - 'binaries'
  - 'package-deb'
  script:
  - make release_docker_images
  parallel:
    matrix:
    - IMAGE_FLAVOR: ubuntu
      TARGET_ARCHS: amd64 arm64 ppc64le
    - IMAGE_FLAVOR: alpine3.12
      TARGET_ARCHS: amd64 arm64 ppc64le
    - IMAGE_FLAVOR: alpine3.13
      TARGET_ARCHS: amd64 arm64 ppc64le
    - IMAGE_FLAVOR: alpine3.14
      TARGET_ARCHS: amd64 arm64 ppc64le
    # we build s390x images on their own, as they don't play nicely when built
    # concurrently: https://gitlab.com/gitlab-org/gitlab-runner/-/issues/28392
    - IMAGE_FLAVOR: ubuntu
      TARGET_ARCHS: s390x
    - IMAGE_FLAVOR: alpine3.12
      TARGET_ARCHS: s390x
    - IMAGE_FLAVOR: alpine3.13
      TARGET_ARCHS: s390x
    - IMAGE_FLAVOR: alpine3.14
      TARGET_ARCHS: s390x

.release_helper_docker_images:
  extends:
  - .release_docker_images_base
  dependencies:
  - 'ecr public token'
  - 'binaries'
  - 'helper images'
  script:
  - make release_helper_docker_images
  parallel:
    matrix:
    - IMAGE_FLAVOR: "alpine"
      IMAGE_TAG: ["", "alpine3.12-"]
    - IMAGE_FLAVOR: "alpine3.13"
      IMAGE_TAG: ["alpine3.13-"]
    - IMAGE_FLAVOR: "alpine3.14"
      IMAGE_TAG: ["alpine3.14-"]
    - IMAGE_FLAVOR: "ubuntu"
      IMAGE_TAG: ["ubuntu-"]

.release_windows_docker_images:
  extends:
  - .windows-dependency-checksums
  stage: release
  variables:
    PUSH_TO_DOCKER_HUB: "true"
    PUSH_TO_ECR_PUBLIC: "true"
    PUBLISH_IMAGES: "true"
    PWSH_VERSION: 7.1.1
  dependencies:
  - 'helper images binaries'
  - 'ecr public token'
  - 'binaries: [windows/386 windows/amd64]'
  script:
  - .\ci\build_release_windows_images.ps1
  retry: 2

.release:
  stage: release
  before_script:
  - source ci/touch_make_dependencies

.release_s3:
  extends:
  - .release
  dependencies:
  - 'helper images'
  - 'helper images binaries'
  - 'test coverage report'
  - 'binaries'
  - 'package-deb'
  - 'package-rpm'
  before_script:
  - source ci/touch_make_dependencies
  - |
    # checking GPG signing support
    if [ -f "$GPG_KEY_PATH" ]; then
      export GPG_KEY=$(cat ${GPG_KEY_PATH})
      export GPG_PASSPHRASE=$(cat ${GPG_PASSPHRASE_PATH})
    else
      echo -e "\033[0;31m****** GPG signing disabled ******\033[0m"
    fi
  script:
  - make release_s3

.release_packagecloud:
  extends:
  - .release
  dependencies:
  - 'package-deb'
  - 'package-rpm'
  script:
  - make release_packagecloud

development S3:
  extends:
  - .release_s3
  - .rules:runner-only:release:development:merge-requests
  environment:
    name: development/s3/${CI_COMMIT_REF_NAME}
    url: https://gitlab-runner-downloads.s3.amazonaws.com/${CI_COMMIT_REF_NAME}/index.html

development docker images:
  extends:
  - .release_docker_images
  - .rules:release:development:merge-requests
  variables:
    PUBLISH_IMAGES: "false"
    PUSH_TO_DOCKER_HUB: "false"
    PUSH_TO_ECR_PUBLIC: "false"

development helper docker images:
  extends:
  - .release_helper_docker_images
  - .rules:release:development:merge-requests
  variables:
    PUBLISH_IMAGES: "false"
    PUSH_TO_DOCKER_HUB: "false"
    PUSH_TO_ECR_PUBLIC: "false"

development servercore1809 helper docker image:
  extends:
  - .windows1809
  - .release_windows_docker_images
  - .rules:release:development:merge-requests
  variables:
    PUBLISH_IMAGES: "false"
    PUSH_TO_DOCKER_HUB: "false"
    PUSH_TO_ECR_PUBLIC: "false"

development servercore2004 helper docker image:
  extends:
  - .windows2004
  - .release_windows_docker_images
  - .rules:release:development:merge-requests
  variables:
    PUBLISH_IMAGES: "false"
    PUSH_TO_DOCKER_HUB: "false"
    PUSH_TO_ECR_PUBLIC: "false"

development servercore20H2 helper docker image:
  extends:
  - .windows20H2
  - .release_windows_docker_images
  - .rules:release:development:merge-requests
  variables:
    PUBLISH_IMAGES: "false"
    PUSH_TO_DOCKER_HUB: "false"
    PUSH_TO_ECR_PUBLIC: "false"

bleeding edge S3:
  extends:
  - .release_s3
  - .rules:release:bleeding-edge
  environment:
    name: bleeding_edge/s3
    url: https://gitlab-runner-downloads.s3.amazonaws.com/${CI_COMMIT_REF_NAME}/index.html

bleeding edge packagecloud:
  extends:
  - .release_packagecloud
  - .rules:release:bleeding-edge
  environment:
    name: bleeding_edge/packagecloud
    url: https://packages.gitlab.com/runner/unstable
  parallel:
    matrix:
    - DIST_FLAVOR:
      - debian
      - ubuntu
      - el
      - fedora

.bleding_edge_docker_images_base:
  extends:
  - .rules:release:bleeding-edge
  environment:
    name: bleeding_edge/docker_images/linux
    url: https://hub.docker.com/r/gitlab/gitlab-runner/tags/

bleeding edge docker images:
  extends:
  - .bleding_edge_docker_images_base
  - .release_docker_images

bleeding edge helper docker images:
  extends:
  - .bleding_edge_docker_images_base
  - .release_helper_docker_images

bleeding edge servercore1809 docker images:
  extends:
  - .windows1809
  - .release_windows_docker_images
  - .rules:release:bleeding-edge
  environment:
    name: bleeding_edge/docker_images/windows1809
    url: https://hub.docker.com/r/gitlab/gitlab-runner/tags/

bleeding edge servercore2004 docker images:
  extends:
  - .windows2004
  - .release_windows_docker_images
  - .rules:release:bleeding-edge
  environment:
    name: bleeding_edge/docker_images/windows2004
    url: https://hub.docker.com/r/gitlab/gitlab-runner/tags/

bleeding edge servercore20H2 docker images:
  extends:
  - .windows20H2
  - .release_windows_docker_images
  - .rules:release:bleeding-edge
  environment:
    name: bleeding_edge/docker_images/windows20H2
    url: https://hub.docker.com/r/gitlab/gitlab-runner/tags/

stable S3:
  extends:
  - .release_s3
  - .rules:release:stable:branch
  environment:
    name: stable/s3
    url: https://gitlab-runner-downloads.s3.amazonaws.com/${CI_COMMIT_REF_NAME}/index.html

stable gitlab release:
  extends:
  - .release
  - .rules:release:stable-or-rc
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  variables:
    CHANGELOG: https://gitlab.com/gitlab-org/gitlab-runner/blob/$CI_COMMIT_TAG/CHANGELOG.md
    S3: https://gitlab-runner-downloads.s3.amazonaws.com/$CI_COMMIT_TAG
  environment:
    name: stable/gitlab
    url: https://gitlab.com/gitlab-org/gitlab-runner/-/releases
  script:
  - echo "Releasing to $S3"
  release:
    name: '$CI_COMMIT_TAG'
    description: 'See [the changelog]($CHANGELOG) :rocket:'
    tag_name: '$CI_COMMIT_TAG'
    ref: '$CI_COMMIT_TAG'
    assets:
      links:
      - name: 'linux amd64'
        url: '$S3/binaries/gitlab-runner-linux-amd64'
      - name: 'linux arm'
        url: '$S3/binaries/gitlab-runner-linux-arm'
      - name: 'linux s390x'
        url: '$S3/binaries/gitlab-runner-linux-s390x'
      - name: 'macOS'
        url: '$S3/binaries/gitlab-runner-darwin-amd64'
      - name: 'Windows 64 bits'
        url: '$S3/binaries/gitlab-runner-windows-amd64.zip'
      - name: 'Windows 32 bits'
        url: '$S3/binaries/gitlab-runner-windows-386.zip'
      - name: 'deb amd64'
        url: '$S3/deb/gitlab-runner_amd64.deb'
      - name: 'deb i386'
        url: '$S3/deb/gitlab-runner_i386.deb'
      - name: 'deb armel'
        url: '$S3/deb/gitlab-runner_armel.deb'
      - name: 'deb armhf'
        url: '$S3/deb/gitlab-runner_armhf.deb'
      - name: 'deb aarch64'
        url: '$S3/deb/gitlab-runner_aarch64.deb'
      - name: 'deb arm64'
        url: '$S3/deb/gitlab-runner_arm64.deb'
      - name: 'rpm amd64'
        url: '$S3/rpm/gitlab-runner_amd64.rpm'
      - name: 'rpm i686'
        url: '$S3/rpm/gitlab-runner_i686.rpm'
      - name: 'rpm arm'
        url: '$S3/rpm/gitlab-runner_arm.rpm'
      - name: 'rpm armhf'
        url: '$S3/rpm/gitlab-runner_armhf.rpm'
      - name: 'rpm arm64'
        url: '$S3/rpm/gitlab-runner_arm64.rpm'
      - name: 'rpm aarch64'
        url: '$S3/rpm/gitlab-runner_aarch64.rpm'
      - name: 'others'
        'url': '$S3/index.html'
      - name: 'docs'
        'url': 'https://docs.gitlab.com/runner/'

stable packagecloud:
  extends:
  - .release_packagecloud
  - .rules:release:stable:branch
  environment:
    name: stable/packagecloud
    url: https://packages.gitlab.com/runner/gitlab-runner
  parallel:
    matrix:
    - DIST_FLAVOR:
      - debian
      - ubuntu
      - raspbian
      - linuxmint
      - el
      - ol
      - fedora

.stable_docker_images_base:
  extends:
  - .rules:release:stable:branch
  environment:
    name: stable/docker_images/linux
    url: https://hub.docker.com/r/gitlab/gitlab-runner/tags/

stable docker images:
  extends:
  - .stable_docker_images_base
  - .release_docker_images

stable helper docker images:
  extends:
  - .stable_docker_images_base
  - .release_helper_docker_images

stable servercore1809 docker images:
  extends:
  - .windows1809
  - .release_windows_docker_images
  - .rules:release:stable:branch
  environment:
    name: stable/docker_images/windows1809
    url: https://hub.docker.com/r/gitlab/gitlab-runner/tags/

stable servercore2004 docker images:
  extends:
  - .windows2004
  - .release_windows_docker_images
  - .rules:release:stable:branch
  environment:
    name: stable/docker_images/windows2004
    url: https://hub.docker.com/r/gitlab/gitlab-runner/tags/

stable servercore20H2 docker images:
  extends:
  - .windows20H2
  - .release_windows_docker_images
  - .rules:release:stable:branch
  environment:
    name: stable/docker_images/windows20H2
    url: https://hub.docker.com/r/gitlab/gitlab-runner/tags/
